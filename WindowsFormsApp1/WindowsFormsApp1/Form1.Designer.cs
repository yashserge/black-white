﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.input = new System.Windows.Forms.GroupBox();
            this.whiteButton = new System.Windows.Forms.Button();
            this.blackButton = new System.Windows.Forms.Button();
            this.control = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.settings = new System.Windows.Forms.GroupBox();
            this.countText = new System.Windows.Forms.Label();
            this.feedText = new System.Windows.Forms.Label();
            this.analyzeFeedText = new System.Windows.Forms.Label();
            this.count = new System.Windows.Forms.TextBox();
            this.feedCount = new System.Windows.Forms.NumericUpDown();
            this.analyzeCount = new System.Windows.Forms.NumericUpDown();
            this.result = new System.Windows.Forms.GroupBox();
            this.mismatchIndexLabel = new System.Windows.Forms.Label();
            this.matchIndexLabel = new System.Windows.Forms.Label();
            this.mismatchLabel = new System.Windows.Forms.Label();
            this.matchLabel = new System.Windows.Forms.Label();
            this.viewTable = new System.Windows.Forms.DataGridView();
            this.colorTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.matchText = new System.Windows.Forms.Label();
            this.dismatchText = new System.Windows.Forms.Label();
            this.form1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.matchIndexText = new System.Windows.Forms.Label();
            this.dismatchIndexText = new System.Windows.Forms.Label();
            this.input.SuspendLayout();
            this.control.SuspendLayout();
            this.settings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.feedCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analyzeCount)).BeginInit();
            this.result.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // input
            // 
            this.input.Controls.Add(this.whiteButton);
            this.input.Controls.Add(this.blackButton);
            this.input.Location = new System.Drawing.Point(6, 106);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(203, 56);
            this.input.TabIndex = 2;
            this.input.TabStop = false;
            this.input.Text = "Ввод";
            // 
            // whiteButton
            // 
            this.whiteButton.Location = new System.Drawing.Point(7, 19);
            this.whiteButton.Name = "whiteButton";
            this.whiteButton.Size = new System.Drawing.Size(75, 23);
            this.whiteButton.TabIndex = 0;
            this.whiteButton.Text = "Белая";
            this.whiteButton.UseVisualStyleBackColor = true;
            this.whiteButton.Click += new System.EventHandler(this.whiteButton_Click);
            // 
            // blackButton
            // 
            this.blackButton.Location = new System.Drawing.Point(122, 19);
            this.blackButton.Name = "blackButton";
            this.blackButton.Size = new System.Drawing.Size(75, 23);
            this.blackButton.TabIndex = 1;
            this.blackButton.Text = "Чёрная";
            this.blackButton.UseVisualStyleBackColor = true;
            this.blackButton.Click += new System.EventHandler(this.blackButton_Click);
            // 
            // control
            // 
            this.control.Controls.Add(this.cancelButton);
            this.control.Controls.Add(this.clearButton);
            this.control.Location = new System.Drawing.Point(215, 106);
            this.control.Name = "control";
            this.control.Size = new System.Drawing.Size(203, 56);
            this.control.TabIndex = 3;
            this.control.TabStop = false;
            this.control.Text = "Управление";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(6, 19);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(122, 19);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 4;
            this.clearButton.Text = "Очистить";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // settings
            // 
            this.settings.Controls.Add(this.countText);
            this.settings.Controls.Add(this.feedText);
            this.settings.Controls.Add(this.analyzeFeedText);
            this.settings.Controls.Add(this.count);
            this.settings.Controls.Add(this.feedCount);
            this.settings.Controls.Add(this.analyzeCount);
            this.settings.Location = new System.Drawing.Point(6, 168);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(203, 140);
            this.settings.TabIndex = 4;
            this.settings.TabStop = false;
            this.settings.Text = "Настройки";
            // 
            // countText
            // 
            this.countText.AutoSize = true;
            this.countText.Location = new System.Drawing.Point(4, 120);
            this.countText.Name = "countText";
            this.countText.Size = new System.Drawing.Size(82, 13);
            this.countText.TabIndex = 4;
            this.countText.Text = "Всего введено";
            // 
            // feedText
            // 
            this.feedText.AutoSize = true;
            this.feedText.Location = new System.Drawing.Point(4, 81);
            this.feedText.Name = "feedText";
            this.feedText.Size = new System.Drawing.Size(125, 13);
            this.feedText.TabIndex = 3;
            this.feedText.Text = "Длина основного фида";
            // 
            // analyzeFeedText
            // 
            this.analyzeFeedText.AutoSize = true;
            this.analyzeFeedText.Location = new System.Drawing.Point(4, 42);
            this.analyzeFeedText.Name = "analyzeFeedText";
            this.analyzeFeedText.Size = new System.Drawing.Size(159, 13);
            this.analyzeFeedText.TabIndex = 2;
            this.analyzeFeedText.Text = "Длина анализирующего фида";
            // 
            // count
            // 
            this.count.Location = new System.Drawing.Point(7, 97);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(75, 20);
            this.count.TabIndex = 0;
            // 
            // feedCount
            // 
            this.feedCount.Location = new System.Drawing.Point(7, 58);
            this.feedCount.Name = "feedCount";
            this.feedCount.Size = new System.Drawing.Size(75, 20);
            this.feedCount.TabIndex = 1;
            this.feedCount.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.feedCount.ValueChanged += new System.EventHandler(this.feedCount_ValueChanged);
            // 
            // analyzeCount
            // 
            this.analyzeCount.Location = new System.Drawing.Point(7, 19);
            this.analyzeCount.Name = "analyzeCount";
            this.analyzeCount.Size = new System.Drawing.Size(75, 20);
            this.analyzeCount.TabIndex = 0;
            this.analyzeCount.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.analyzeCount.ValueChanged += new System.EventHandler(this.analyzeCount_ValueChanged);
            // 
            // result
            // 
            this.result.Controls.Add(this.dismatchIndexText);
            this.result.Controls.Add(this.matchIndexText);
            this.result.Controls.Add(this.dismatchText);
            this.result.Controls.Add(this.matchText);
            this.result.Controls.Add(this.mismatchIndexLabel);
            this.result.Controls.Add(this.matchIndexLabel);
            this.result.Controls.Add(this.mismatchLabel);
            this.result.Controls.Add(this.matchLabel);
            this.result.Location = new System.Drawing.Point(215, 168);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(203, 140);
            this.result.TabIndex = 6;
            this.result.TabStop = false;
            this.result.Text = "Результат";
            // 
            // mismatchIndexLabel
            // 
            this.mismatchIndexLabel.AutoSize = true;
            this.mismatchIndexLabel.Location = new System.Drawing.Point(119, 97);
            this.mismatchIndexLabel.Name = "mismatchIndexLabel";
            this.mismatchIndexLabel.Size = new System.Drawing.Size(77, 13);
            this.mismatchIndexLabel.TabIndex = 3;
            this.mismatchIndexLabel.Text = "mismatchIndex";
            // 
            // matchIndexLabel
            // 
            this.matchIndexLabel.AutoSize = true;
            this.matchIndexLabel.Location = new System.Drawing.Point(3, 97);
            this.matchIndexLabel.Name = "matchIndexLabel";
            this.matchIndexLabel.Size = new System.Drawing.Size(62, 13);
            this.matchIndexLabel.TabIndex = 2;
            this.matchIndexLabel.Text = "matchIndex";
            // 
            // mismatchLabel
            // 
            this.mismatchLabel.AutoSize = true;
            this.mismatchLabel.Location = new System.Drawing.Point(119, 42);
            this.mismatchLabel.Name = "mismatchLabel";
            this.mismatchLabel.Size = new System.Drawing.Size(51, 13);
            this.mismatchLabel.TabIndex = 1;
            this.mismatchLabel.Text = "mismatch";
            // 
            // matchLabel
            // 
            this.matchLabel.AutoSize = true;
            this.matchLabel.Location = new System.Drawing.Point(3, 42);
            this.matchLabel.Name = "matchLabel";
            this.matchLabel.Size = new System.Drawing.Size(36, 13);
            this.matchLabel.TabIndex = 0;
            this.matchLabel.Text = "match";
            // 
            // viewTable
            // 
            this.viewTable.AllowUserToAddRows = false;
            this.viewTable.AllowUserToDeleteRows = false;
            this.viewTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.viewTable.Location = new System.Drawing.Point(0, 0);
            this.viewTable.Name = "viewTable";
            this.viewTable.ReadOnly = true;
            this.viewTable.Size = new System.Drawing.Size(423, 100);
            this.viewTable.TabIndex = 5;
            // 
            // matchText
            // 
            this.matchText.AutoSize = true;
            this.matchText.Location = new System.Drawing.Point(3, 26);
            this.matchText.Name = "matchText";
            this.matchText.Size = new System.Drawing.Size(71, 13);
            this.matchText.TabIndex = 4;
            this.matchText.Text = "Совпадения:";
            // 
            // dismatchText
            // 
            this.dismatchText.AutoSize = true;
            this.dismatchText.Location = new System.Drawing.Point(119, 26);
            this.dismatchText.Name = "dismatchText";
            this.dismatchText.Size = new System.Drawing.Size(84, 13);
            this.dismatchText.TabIndex = 5;
            this.dismatchText.Text = "Несовпадения:";
            // 
            // form1BindingSource
            // 
            this.form1BindingSource.DataSource = typeof(WindowsFormsApp1.Form1);
            // 
            // matchIndexText
            // 
            this.matchIndexText.AutoSize = true;
            this.matchIndexText.Location = new System.Drawing.Point(3, 81);
            this.matchIndexText.Name = "matchIndexText";
            this.matchIndexText.Size = new System.Drawing.Size(82, 13);
            this.matchIndexText.TabIndex = 6;
            this.matchIndexText.Text = "Номер дороги:";
            // 
            // dismatchIndexText
            // 
            this.dismatchIndexText.AutoSize = true;
            this.dismatchIndexText.Location = new System.Drawing.Point(122, 75);
            this.dismatchIndexText.Name = "dismatchIndexText";
            this.dismatchIndexText.Size = new System.Drawing.Size(82, 13);
            this.dismatchIndexText.TabIndex = 7;
            this.dismatchIndexText.Text = "Номер дороги:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 312);
            this.Controls.Add(this.result);
            this.Controls.Add(this.viewTable);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.control);
            this.Controls.Add(this.input);
            this.Name = "Form1";
            this.Text = "Чёрная и Белая";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.input.ResumeLayout(false);
            this.control.ResumeLayout(false);
            this.settings.ResumeLayout(false);
            this.settings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.feedCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analyzeCount)).EndInit();
            this.result.ResumeLayout(false);
            this.result.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button whiteButton;
        private System.Windows.Forms.Button blackButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.GroupBox input;
        private System.Windows.Forms.GroupBox control;
        private System.Windows.Forms.GroupBox settings;
        private System.Windows.Forms.NumericUpDown feedCount;
        private System.Windows.Forms.NumericUpDown analyzeCount;
        private System.Windows.Forms.BindingSource form1BindingSource;
        private System.Windows.Forms.GroupBox result;
        private System.Windows.Forms.Label countText;
        private System.Windows.Forms.Label feedText;
        private System.Windows.Forms.Label analyzeFeedText;
        private System.Windows.Forms.TextBox count;
        private System.Windows.Forms.BindingSource dataSet1BindingSource;
        private System.Windows.Forms.DataGridView viewTable;
        private System.Windows.Forms.BindingSource colorTableBindingSource;
        private System.Data.DataTable table;
        private System.Windows.Forms.Label mismatchLabel;
        private System.Windows.Forms.Label matchLabel;
        private System.Windows.Forms.Label mismatchIndexLabel;
        private System.Windows.Forms.Label matchIndexLabel;

        private int match, matchIndex;
        private int mismatch, mismatchIndex;
        private System.Windows.Forms.Label dismatchText;
        private System.Windows.Forms.Label matchText;
        private System.Windows.Forms.Label dismatchIndexText;
        private System.Windows.Forms.Label matchIndexText;
    }
}

