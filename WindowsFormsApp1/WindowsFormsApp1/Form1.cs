﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form, INotifyPropertyChanged
    {
        public int Match
        {
            get { return this.match; }
            set
            {
                this.match = value;
                OnPropertyChanged("Match");
            }
        }

        public int Mismatch
        {
            get { return this.mismatch; }
            set
            {
                this.mismatch = value;
                OnPropertyChanged("Mismatch");
            }
        }

        public int MatchIndex
        {
            get { return this.matchIndex; }
            set
            {
                this.matchIndex = value;
                OnPropertyChanged("MatchIndex");
            }
        }

        public int MismatchIndex
        {
            get { return this.mismatchIndex; }
            set
            {
                this.mismatchIndex = value;
                OnPropertyChanged("MismatchIndex");
            }
        }

        protected virtual void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public Form1()
        {
            InitializeComponent();

            matchLabel.DataBindings.Add(new Binding("Text", this, "Match"));
            mismatchLabel.DataBindings.Add(new Binding("Text", this, "Mismatch"));
            matchIndexLabel.DataBindings.Add(new Binding("Text", this, "MatchIndex"));
            mismatchIndexLabel.DataBindings.Add(new Binding("Text", this, "MismatchIndex"));

            Match = Mismatch = 0;
            MatchIndex = MismatchIndex = -1;
            count.Text = "0";

            //MessageBox.Show("Form1()");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.table = new System.Data.DataTable();

            this.table.Columns.Add("id", typeof(int));
            this.table.Columns.Add("color", typeof(string));

            //table.Rows.Add(1, "black");
            //table.Rows.Add(2, "white");

            this.viewTable.DataSource = table;

            //MessageBox.Show("Form1_Load()");
        }

        private void updateCounts(object sender, EventArgs e)
        {
            int count, max;

            max = 0;
            for (int i = 1; i < this.table.Rows.Count; ++i)
            {
                count = 0;
                int j = 0;
                while (j < this.analyzeCount.Value &&
                       j < this.feedCount.Value &&
                       i + j < this.table.Rows.Count &&
                       this.table.Rows[j][1] == this.table.Rows[j + i][1])
                {
                    ++count;
                    ++j;
                }

                if (count > max)
                {
                    max = count;
                    Match = count;
                    MatchIndex = i;
                }
            }

            max = 0;
            for (int i = 1; i < this.table.Rows.Count; ++i)
            {
                count = 0;
                int j = 0;
                while (j < this.analyzeCount.Value &&
                       j < this.feedCount.Value &&
                       i + j < this.table.Rows.Count &&
                       this.table.Rows[j][1] != this.table.Rows[j + i][1])
                {
                    ++count;
                    ++j;
                }

                if (count > max)
                {
                    max = count;
                    Mismatch = count;
                    MismatchIndex = i;
                }
            }

            this.count.Text = viewTable.Rows.Count.ToString();
        }

        private void whiteButton_Click(object sender, EventArgs e)
        {
            //table.Rows.Add(viewTable.Rows.Count + 1, "white");
            DataRow row = table.NewRow();
            row[0] = 1;
            row[1] = "white";
            table.Rows.InsertAt(row, 0);
 
            viewTable.CurrentCell = viewTable.Rows[0].Cells[0];

            for (int i = 1; i < this.table.Rows.Count; ++i)
                table.Rows[i][0] = i + 1;

            updateCounts(sender, e);

            //MessageBox.Show("Я - белая кнопка.");
        }

        private void blackButton_Click(object sender, EventArgs e)
        {
            //table.Rows.Add(viewTable.Rows.Count + 1, "black");
            DataRow row = table.NewRow();
            row[0] = 1;
            row[1] = "black";
            table.Rows.InsertAt(row, 0);

            viewTable.CurrentCell = viewTable.Rows[0].Cells[0];

            for (int i = 1; i < this.table.Rows.Count; ++i)
                table.Rows[i][0] = i + 1;

            updateCounts(sender, e);

            //MessageBox.Show("Я - чёрная кнопка.");
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (table.Rows.Count > 0)
                table.Rows[0].Delete();

            int i = 0;
            foreach (DataRow row in table.Rows)
            {
                row[0] = ++i;
            }

            updateCounts(sender, e);

            //MessageBox.Show("Последний ввод отменён.");
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            if (table.Rows.Count > 0)
                table.Rows.Clear();

            Match = Mismatch = 0;
            MatchIndex = MismatchIndex = -1;
            count.Text = "0";

            //MessageBox.Show("Всё очищено!");
        }

        private void analyzeCount_ValueChanged(object sender, EventArgs e)
        {
            if (this.analyzeCount.Value > this.table.Rows.Count)
                this.analyzeCount.Value = this.table.Rows.Count;

            updateCounts(sender, e);
        }

        private void feedCount_ValueChanged(object sender, EventArgs e)
        {
            if (this.feedCount.Value > this.table.Rows.Count)
                this.feedCount.Value = this.table.Rows.Count;

            updateCounts(sender, e);
        }
    }
}
